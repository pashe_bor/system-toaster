# system-toast



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description | Type                                                           | Default     |
| ---------------- | ------------------ | ----------- | -------------------------------------------------------------- | ----------- |
| `content`        | `content`          |             | `string`                                                       | `undefined` |
| `header`         | `header`           |             | `string`                                                       | `undefined` |
| `image`          | `image`            |             | `string`                                                       | `undefined` |
| `maxContentSize` | `max-content-size` |             | `number`                                                       | `undefined` |
| `position`       | `position`         |             | `"left-bottom" \| "left-top" \| "right-bottom" \| "right-top"` | `undefined` |
| `timer`          | `timer`            |             | `number`                                                       | `0`         |
| `toastID`        | `toast-i-d`        |             | `string`                                                       | `undefined` |
| `type`           | `type`             |             | `string`                                                       | `'info'`    |


## Events

| Event         | Description | Type                                |
| ------------- | ----------- | ----------------------------------- |
| `removeToast` |             | `CustomEvent<{ toastID: string; }>` |


## Dependencies

### Used by

 - [system-toaster](../..)

### Depends on

- [toast-content](content)

### Graph
```mermaid
graph TD;
  system-toast --> toast-content
  system-toaster --> system-toast
  style system-toast fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
