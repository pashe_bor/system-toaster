import {
    h,
    Component,
    Prop,
    State,
    Event
} from '@stencil/core';

import { truncate } from '../../../__utils__/textFormat';

import { EventEmitter } from '@stencil/core';

@Component({
    tag: 'toast-content',
    styleUrl: 'content.scss',
    shadow: false
})
export class ToastContent {
    @Event() expandToast: EventEmitter<{ isExpanded: boolean }>;

    @State() isExpanded = false;

    @Prop() content?: string;
    @Prop() maxSize?: number;

    expandHandler = (event): void => {
        event.stopPropagation();

        const { isExpanded, expandToast } = this;
        this.isExpanded = !isExpanded;

        expandToast.emit({ isExpanded: !isExpanded });
    }

    render() {
        const {
            content,
            maxSize,
            isExpanded,
            expandHandler
        } = this;

        const moreBtnClassList = {
            ['system-toast-content__more-btn']: true,
            ['system-toast-content__more-btn-is-expanded']: this.isExpanded
        };

        if (content) {
            return (
                <p class="system-toast-content">
                    {
                        (maxSize && maxSize < content.length)
                            ? (
                                <span onClick={expandHandler} style={{ cursor: 'pointer' }}>
                                    {
                                        isExpanded
                                            ? content
                                            : truncate(content, maxSize)
                                    }
                                    <button class={moreBtnClassList} onClick={expandHandler}>
                                        <svg width="16" height="12" viewBox="4 6 16 12">
                                            <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"/>
                                        </svg>
                                    </button>
                                </span>
                            ) : content
                    }
                </p>
            );
        }

        return null;
    }
}