# toast-content



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description | Type     | Default     |
| --------- | ---------- | ----------- | -------- | ----------- |
| `content` | `content`  |             | `string` | `undefined` |
| `maxSize` | `max-size` |             | `number` | `undefined` |


## Events

| Event         | Description | Type                                    |
| ------------- | ----------- | --------------------------------------- |
| `expandToast` |             | `CustomEvent<{ isExpanded: boolean; }>` |


## Dependencies

### Used by

 - [system-toast](..)

### Graph
```mermaid
graph TD;
  system-toast --> toast-content
  style toast-content fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
