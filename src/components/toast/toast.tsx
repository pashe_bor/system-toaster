import {
  h,
  Component,
  Prop,
  Event,
  Element,
  State,
  Listen
} from '@stencil/core';

import { POSITION_NAMES, TOAST_TYPES } from '../../__constants__';

import { EventEmitter, ModeStyles } from '@stencil/core';
import { ToasterAlignmentType} from '../../app';

@Component({
  tag: 'system-toast',
  styleUrl: 'toast.scss',
  shadow: false
})
export class SystemToaster {
  private _timerID: ReturnType<typeof setTimeout> = null;

  @Element() $toastElement: HTMLDivElement;

  @Event() removeToast: EventEmitter<{ toastID: string }>;

  @State() maxHeight: string = '';
  @State() overflow: string = '';
  @State() isClosing: boolean = false;
  @State() isSlideIn: boolean = false;
  @State() isExpanded: boolean = false;

  @Prop() toastID: string;
  @Prop() header?: string;
  @Prop() content?: string;
  @Prop() image?: string;
  @Prop() timer: number = 0;
  @Prop() type: string = 'info';
  @Prop() position: ToasterAlignmentType;
  @Prop() maxContentSize?: number;

  componentDidRender() {
    const { timer, isClosing } = this;

    if (!isClosing) this.setMaxHeight();

    if (timer) {
      this._timerID = setTimeout(this.onCloseHandler, timer);
    }
  }

  componentDidUpdate() {
    if (this.isClosing) return;

    this.setMaxHeight(true);
  }

  disconnectedCallback() {
    this._timerID && clearTimeout(this._timerID);
  }

  private setMaxHeight = (isUpdate?: boolean): void => {
    const { getHeight, isClosing } = this,
      height = isClosing ? '0' : `${getHeight()}px`;

    if (this.maxHeight && !isClosing && !isUpdate) return;

    this.maxHeight = height;
  };

  private getHeight = (): number =>
      this.$toastElement?.scrollHeight;


  private setPosition = (): ModeStyles => {
    const { $toastElement } = this,
      topPosition = $toastElement?.getBoundingClientRect().top,
      bottomPosition = $toastElement?.getBoundingClientRect().bottom,
      screenHeight = window.innerHeight;

    const position: ModeStyles = {};

    if (topPosition < 0 || bottomPosition > screenHeight) {
      position.zIndex = '1';
      position.animation = 'none';
    }

    if (topPosition < screenHeight) {
      position.transform = `translateY(${Math.abs(topPosition)}px)`;
    }

    if (bottomPosition > screenHeight) {
      position.transform = `translateY(${screenHeight - bottomPosition}px)`;
    }

    return position;
  }

  private onCloseHandler = (): void => {
    this.isClosing = true;
    this._timerID = null;
  };

  private onAnimationEnd = (): void => {
    const { removeToast, toastID, isClosing } = this;

    if (isClosing) {
      this.setMaxHeight();

      removeToast.emit({ toastID });
    }
  };

  private onTransitionEnd = (): void => {
    const { isClosing } = this;

    this.isSlideIn = !isClosing;

    if (isClosing) return;

    this.overflow = '';
  }

  @Listen('expandToast')
  onExpandToastHandler(payload: CustomEvent<{ isExpanded: boolean }>) {
    const { detail } = payload;

    this.isExpanded = detail.isExpanded;
    this.overflow = detail.isExpanded ? 'hidden' : '';
  }

  render() {
    const {
      header,
      content,
      image,
      type = TOAST_TYPES.INFO,
      position,
      maxHeight,
      overflow,
      maxContentSize,
      isClosing,
      isSlideIn,
      isExpanded,
      setPosition,
      onAnimationEnd,
      onTransitionEnd,
      onCloseHandler
    } = this;

    let classList = {
          [`system-toast system-toast--${type}`]: true,
          [`system-toast--${position}`]: true,
          [`${`system-toast--${
              (
                  position === POSITION_NAMES.BOTTOM_LEFT ||
                  position === POSITION_NAMES.LEFT_TOP
              )
                  ? 'left'
                  : 'right'
          }-slide-out`}`]: isClosing,
          [`${`system-toast--${
              (
                  position === POSITION_NAMES.BOTTOM_LEFT ||
                  position === POSITION_NAMES.LEFT_TOP
              )
                  ? 'left'
                  : 'right'
          }-slide-in`}`]: isSlideIn
        },
        closeClassList = `system-toast__close system-toast__close--${
            (
                position === POSITION_NAMES.BOTTOM_LEFT ||
                position === POSITION_NAMES.LEFT_TOP
            )
                ? 'right'
                : 'left'
        }`;

    return (
        <div
            class={classList}
            style={isExpanded ? {maxHeight, overflow, ...setPosition()} : {maxHeight}}
            onTransitionEnd={onTransitionEnd}
            onAnimationEnd={onAnimationEnd}
            onMouseOver={(event) => event.preventDefault()}
        >
          {
            !overflow && (
                <button class={closeClassList} onClick={onCloseHandler}>
                  <svg
                      height="24px"
                      width="24px"
                      viewBox="0 0 24 24"
                  >
                    <path
                        d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"/>
                  </svg>
                </button>
            )
          }
          {
            image && <img class="system-toast__image" loading="lazy" src={image} alt="Toast image" />
          }
          <div class="system-toast__group">
            {header && <h2 class="system-toast__title">{header}</h2>}
            <toast-content maxSize={maxContentSize} content={content}/>
          </div>
        </div>
    );
  }
}
