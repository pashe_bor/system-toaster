# system-toaster



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description | Type                                                           | Default      |
| ---------------- | ------------------ | ----------- | -------------------------------------------------------------- | ------------ |
| `alignment`      | `alignment`        |             | `"left-bottom" \| "left-top" \| "right-bottom" \| "right-top"` | `'left-top'` |
| `maxContentSize` | `max-content-size` |             | `number`                                                       | `undefined`  |
| `theme`          | `theme`            |             | `string`                                                       | `'default'`  |
| `toasts`         | `toasts`           |             | `ToastType[] \| string`                                        | `undefined`  |


## Methods

### `addToast(toast: ToastType | ToastType[]) => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [system-toast](./components/toast)

### Graph
```mermaid
graph TD;
  system-toaster --> system-toast
  system-toast --> toast-content
  style system-toaster fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
