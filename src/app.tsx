import {
  Component,
  Prop,
  Watch,
  Method,
  Listen,
  State,
  h
} from '@stencil/core';

import { getHashFromString } from './__utils__/textFormat';

import { POSITION_NAMES, TOAST_POSITION } from './__constants__';

export type ToasterAlignmentType = 'left-top' | 'right-top' | 'left-bottom' | 'right-bottom';
export type ToastType = {
  toastID?: string;
  type?: 'error' | 'warning' | 'success' | 'info',
  header?: string;
  content?: string;
  image?: string;
  timer?: number;
  maxContentSize?: number;
}
export type SystemToasterProps = {
  alignment: ToasterAlignmentType;
  toasts?: ToastType[]
  theme?: 'default' | 'material',
  maxContentSize?: number;
}

@Component({
  tag: 'system-toaster',
  styleUrl: 'app.scss',
  shadow: true,
})
export class SystemToaster {
  private CLEAR_TIME = 1000;

  @State() _toasts: ToastType[] = null;

  @Prop() alignment: ToasterAlignmentType = 'left-top';
  @Prop() toasts?: ToastType[] | string;
  @Prop() maxContentSize?: number;
  @Prop() theme?: string = 'default';

  @Watch('toasts')
  toastsDataWatcher(newValue: ToastType[] | string) {
    this.setToasts(
      typeof newValue === 'string'
        ? JSON.parse(newValue)
        : newValue
    );
  }

  @Method()
  public async addToast(toast: ToastType | ToastType[] ): Promise<void> {
    const addToastID = (toast: ToastType): { toastID: string } => ({
      toastID: toast.toastID || getHashFromString(32, toast.header)
    }), toasts = Array.isArray(toast)
      ? toast.map((item) => ({ ...item, ...addToastID(item) }))
      : [{ ...toast, ...addToastID(toast)}]

    this.setToasts(toasts);
  }

  @Listen('removeToast')
  removeToastHandler(toast: CustomEvent<{ toastID: string }>) {
      const { _toasts, CLEAR_TIME } = this,
          { detail } = toast;

      setTimeout(() => {
        this._toasts = _toasts.filter(({ toastID }) => toastID !== detail.toastID);
      }, CLEAR_TIME);
  };

  private setToasts = (toasts: ToastType[]): void => {
    const { _toasts } = this;

    if (Array.isArray(_toasts)) {
      this._toasts = [..._toasts, ...toasts];
    } else {
      this._toasts = toasts;
    }
  };

  componentWillLoad() {
    this.toastsDataWatcher(this.toasts);
  }

  render() {
    const {
      maxContentSize,
      theme
    } = this;

    const containerStyle = TOAST_POSITION[this.alignment] || null,
      isListBottom = this.alignment === POSITION_NAMES.BOTTOM_LEFT ||
        this.alignment === POSITION_NAMES.BOTTOM_RIGHT;

    const toastsList = this._toasts?.map(
      (
        {
          type,
          header,
          content,
          image,
          timer,
          toastID
        }
      ) =>
        <system-toast
          key={toastID}
          toastID={toastID}
          header={header}
          content={content}
          type={type}
          timer={timer}
          position={this.alignment}
          image={image}
          maxContentSize={maxContentSize}
        />);

    const toasterClassList = {
      ['system-toaster']: true,
      [`system-toaster--${theme}`]: !!theme
    }

    return (
      <div class={toasterClassList}>
        <div class="system-toaster__container" style={containerStyle}>
          {
            isListBottom
              ? toastsList
              : toastsList?.reverse()
          }
        </div>
      </div>
    );
  }
}
