export const getHashFromString = (length, chars) => {
  let result = '';
  for (let i = length; i > 0; --i) {
    result += chars[Math.floor(Math.random() * chars.length)];
  }

  return result.replace(/ /g, '');
}

export const truncate = (text: string, length: number): string =>  (
    text.length > length
        ? `${text.slice(0, length)}...`
        : text
);
