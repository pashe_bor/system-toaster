export const POSITION_NAMES: {
  LEFT_TOP: 'left-top',
  RIGHT_TOP: 'right-top',
  BOTTOM_LEFT: 'left-bottom',
  BOTTOM_RIGHT: 'right-bottom'
} = Object.freeze({
  LEFT_TOP: 'left-top',
  RIGHT_TOP: 'right-top',
  BOTTOM_LEFT: 'left-bottom',
  BOTTOM_RIGHT: 'right-bottom'
});

export const TOAST_POSITION: Record<string, { [key: string]: string }> = Object.freeze({
  [POSITION_NAMES.LEFT_TOP]: {
    top: '10px',
    left: '10px'
  },
  [POSITION_NAMES.RIGHT_TOP]: {
    top: '10px',
    right: '10px'
  },
  [POSITION_NAMES.BOTTOM_LEFT]: {
    bottom: '10px',
    left: '10px'
  },
  [POSITION_NAMES.BOTTOM_RIGHT]: {
    bottom: '10px',
    right: '10px'
  }
});

export const TOAST_TYPES = Object.freeze({
  ERROR: 'error',
  INFO: 'info',
  WARNING: 'warning',
  SUCCESS: 'success'
});


