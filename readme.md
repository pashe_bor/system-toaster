![](/assets/logo-stencil.svg)

Простой модуль всплывающих уведомлений (`тостов`) на технологии Stencil.js c темами и простой настройкой.
Решение, которое "просто работает": добавьте модуль в свой проект и идите дальше.

![](/assets/system-taoster.gif)

Установка и применение
----------------------

Добавьте файл `.npmrc` в корень проекта с содержимым:
```code
@karusel:registry=https://art.x5.ru/artifactory/api/npm/npm-karusel-dev/
```

Установите модуль используя команду:

```bash
npm install @karusel/system-toaster
```

Далее подключите модуль к вашему проекту

```html
<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
      <script type="module" src="./node_modules/@karusel/system-toaster/www/build/system-toaster.esm.js"></script>
      <script nomodule src="./node_modules/@karusel/system-toaster/www/build/system-toaster.js"></script>
  </head>
  <body>
    <button id="toastBtn" >Add toast</button>
    <system-toaster alignment="right-top" max-content-size="50" theme="material"></system-toaster>
    <script>
      const toaster = document.querySelector('system-toaster'),
        addToastBtn = document.getElementById('toastBtn');

      addToastBtn.addEventListener('click', () => {
          const toastBody = {
              header: 'Hello world!',
              content: 'Lorem ipsum',
              image: 'image.png'
              type: 'success'
          };

          toaster.addToast(toastBody);
      });
    </script>
  </body>
</html>
```

Параметры инициализации элемента' (**`<system-toaster>`**)
-----------------------------------
| Свойство | Тип | Знач. по умолчанию | Описание |
| --- | --- | --- | --- |
| **`alignment`** | `'left-top' 'right-top' 'left-bottom' 'right-bottom'` | `right-top` | Положение относительно углов экрана. |
| **`toasts`** | `{header?: string; content?: string; type?: string; timer?: number; image?: string}[]` | `null` | Массив обектов нотификаций при создании компонента. |
| **`maxContentSize`** | `Number` | `null` | Количество отображаемых символов для св-ва `content` объекта нотификации. |
| **`theme`** | `'default' 'material'` | `default` | Темы выпадающих уведомлений. |

Параметры вызова нотификации (**`addToast`**)
-----------------------------------

Описание передаваемого объекта типа `ToastType` в аргументы метода **`addToast`**:

| Свойство | Тип | Знач. по умолчанию | Описание |
| --- | --- | --- | --- |
| **`toastID`** | `String` | `null` | Уникальный идентификатор уведомления(необязательный параметр). |
| **`type`** | `'error' 'warning' 'success' 'info'` | `info` | Тип уведомления(необязательный параметр). |
| **`header`** | `String` | `null` | Заголовок уведомления(необязательный параметр). |
| **`content`** | `String` | `null` | Тело(текст) уведомления(необязательный параметр). |
| **`image`** | `String` | `null` | Изображение (необязательный параметр). |
| **`timer`** | `Number` | `null` | Время отображения на экране в миллисекундах(необязательный параметр). |
| **`maxContentSize`** | `Number` | `null` | Количество отображаемых символов для св-ва `content` объекта нотификации(необязательный параметр). |

Интеграция в Vue
-----------------
1. Добавьте файл инициализации приложения следующие строки:
```js
import { createApp } from 'vue'
import App from './App.vue'

import { applyPolyfills, defineCustomElements } from '@karusel/system-toaster/loader';

applyPolyfills().then(() => {
  defineCustomElements();
});

createApp(App).mount('#app')
```

2. Пример вызова в коде:
```vue
<template>
  <system-toaster ref="toaster" alignment="right-bottom" theme="material" />
  <button @click="onAddToastHandler">Add toast</button>
</template>

<script>

export default {
  name: 'App',
  methods: {
    onAddToastHandler() {
      const { toaster } = this.$refs;

      toaster.addToast({
        header: 'Test',
        content: 'Lorem ipsum',
        type: 'success',
        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABI1BMVEX////l+P9FQTz/zN3j4+L/5e7/+s///OVCPjn/sMo3Mizr///n+v//zt//0eM2MSs7NzE/OzX/6vMyLSY3NS75+fnp6ejt+v81LSVKRkE2NC0/OTPLysk9PDX09PT3/f+urav/4uz/1eNwbWqbmZfZ2NeEgn+6x8tYVVFjYFzBrrP/3unBwL8uKCGos7bY6e98enft1d2XlZPywtKZi42He3ywn6LfydCCbnG4lqDVrLi1tLLDwsFnZGFjV1aMiofQ4OaRmZudp6mmlpnMpbGLdHnVwMbmucf/+9mdgoiTj3mpi5Pw3OpmZ2XF09h7gIC2s6PMyreBfXPt6tWcmYvf28ibmIvPyqp7d23l4LpbV02gnIO9uJsoLiNrZ1qalX77vdMHbrmNAAAX90lEQVR4nO1dCXvaSNIO8hGNDiQhIAgZgo3BwjEGH7EdH2A7TrzZJPZkZufcnd35/7/i66rW0Qg16ICAv8f1PDNxYlvoVVVXvXV068WLZ3mWZ3mWZ3mWZ3mWZFIqr7ZAVsulRd/K7KW+0eg7lmhUJU2TbNFy+o2N/0cwW9tOV9MNURQ8EUVD17qdxuqib20WsrrtmHqAjRVRNzuN+qJvMKNs9HUfHtFbkUrN8P9NEodPWZEbjmb44AyneXyyC3Jy3LRqRd39lq61n6oeWx2TqsqoWs2Tj5sFVg4+vm8KVQrS1hqrGxcNIheXrfqTcUCloYT3LxaNt7sHBNPaqADK3WZRR4y6qUk6EUkzdaf9NBzQqoP3Llatk4MxdAHKg+OqHvY/hqQ7jfKiAUyTCzRQUXd2ufBckJvHhhHlZfutRWOYKNsm2p41DR9iPHhb1fVitVok/1WLnps1zP4SGysFaL7dnI4PMe4ev9/9eEDklPjZAUFJHZC+vWggPNnW0NB24+FbQ6/jKhv+JA6oSoOo5CynqV6ABg3rdBrAANfYNw7eW0VcyGZj0WgipKUjwIMpAAunu8fEODk/Vlg7EdDJmsNF4xkXhzx8cRpA4l6Mql7Tq8IxZ7GSn8DVLPUXDSgshxKoMNpENzffgGxurh0INTcwFB3ewyjsYhzRr5aL5awCwOqYk9l8s7WlrgQy8ABOMujCAdIGuzM83G5slJeEvfbJYzfejt7ymxFwRNS7KlI1yTQmQ9xsFsHf2Laua6bV3F4C19qCQFHcHMEXgkck/4GoUN+ul1aH2hS3hBD97EuyDhdNA9o2AXgS3O/m1hg8QDgQBdHBX9g2J0MsvA0gYpTtXm0sEmDdBD+6ORnfCEJKDyZp8YouWS+fJHSuuUBjbRA/U3zv3eybaHzUSkXLTR88LW6BEEcbRrjpILTd903dzScN/XBhCCEWGgfuneV5AFfyn4lebG9FIcTaIPjuKMrCKcaMY8J/Pr6tFV06t6DlCEZqXBUiFKiqrL/JvyOLS/NtjUI8Y56I+oaFuAuet/ixQOjc2olD6Zy0txCEG5LvZxiAaj6vHh0dkf97KNU7gpC5R1yL1TtW6epWoMjCW6JyY0C/JnQOF6a5kMRjmwTo6ukoQDV/9O5sYFnW4OzdUZ5iVO+JVvRG8ItDHTxUKKxs+RAPBKK3quuiIZ9EiItYjE2yYqqFUYArn2rFGhoW+fPTUZ7+I9GhwXJqWMDFd6GVq26O2Kke/BXTZG0BEB0IAgUWYP7cCvgZWWwCmqK6YhGELKVuQZgZjHEDfznCsyPOxnc+lrEQQ61bIjK2TQYgqo/QkRoFKhY/o6IgIHbY3+2Qn6vdj0Hc8iABQdUP/JV5gCHE/N7upky9+ppvbOqRBQBrVevsw5lVRZDVd8Tj5M8gILJUek+KMFO4hOdsQIkB33UhSt+5JLcKd/G+EBAZ5J9C8cN9HuT+Ayq0en50/o7oUNBZhBBoRgJGCOIBUh9fiQQiPDzD+b6ZFSAkwSK4uSMLNPjO86BkUaLLKVYxppkjURvW8PhC9P1N4VhnVyIYLngfaS5LsdziEArU4QnjRyEo1D4FisnfszRaG6HQJO0SraO8Og5SVYHPHeCvBkpcK5wgxNlz1I2OruvCMOrCZTC94wCQeg4IPzOmh2TGE+mC/eW2AW3Fs7uVfJQiXYtn0xayNiGKOrMG2OiioRmmtT2mSfCl7FpSj4rIVYJbVlVYgIKhoWscsbA+supaVfh0H6FHMIgasAKWzuFSlLZbM83+N7QgUzOvwt0FXEssvTzDu2KVCJzbGLZa5ObskTJax2sp1mpnd1EY81D5qJ7nA6pT2C1iqYBk/4eXs/KqcB+i7lbeRclo7rHODOJyjUV4T7yn4Yz8S5HyNfIwjCZ7ZdCq2wc3ioO7/JhbRTKLJuLrsXDltSdtTbyaSTsZqYf146+W5II0JKkdOAzgpcVz5vnnz4Vijf0HdK/A15rgWJgrb8CVrxz3umLRuVPDGFXwzOKRytC5A5NJ/6VZdHMaBIL07eXLl99+EjQ/57a8CtGlNOo7AdH5kTryd+A9fepYTObKQwP9YmsoSBRjdRC21fynms8KXDUW3qPnEv1uTub0H5Sk//wS5Ocff/cmEERd6+CSLI+zy9HEkCB0KEK4Ujcwq7oExgEWX244pqvHwfmIX1Xv/wpYgbcawXOJA8u7FUMbZrPVC3Ij+r9euvLzj47mg7SvyJK0otllyErBxcCVzOCBH8Kj83KFS0cT6Xo8O2Ixgq+peiayFWQd4lXh9KTjdnN0K1Olqgz1XvPHl7788qsTLEmzDS5/1EzDCO9rNEyAU9Yuveu28LqBO9zoUFutFT+tMNHnfiCcrXiQtwIlmqcFppvTzZRVQaoqaN9eMvLtJ8tfkvTPI74SMeRDdg/9Gz/k1yE91EeCx2WH6rFo3QVqVFVWp1uU2RS9EnRh88TCbo7Uz2CpdQHDxW//+AeD8edvv5tSMBQ0SYmQVKCyygZjltDrEMXQbV0IEl2OZ0eMGtmLUS0CsXHZHIwFwH3onQwQyxJC/GN9ncVIlmQnGA2qcleielRziVaJ+D+j7V4UwlpErret25QCvOMwuS2Pkhe9RknhowB2ZFsZco4WABENAjEE8pd//SlRkMYgknetuA6fsrW2aXQ9n9DoGpFVl3KfmkaVVSMrEBchxzCafvpPWx12JwNEKKgRz44QwyB/tXD1FDl2SomqgR6ltN0OtHbRbnA+zEFTrbGrkb0gYAKepTNJBxaq9CxtR9rF/nPdkxGQ3/4LVhKVrUOCCNTSbk//iEBK25TqFz+sREHccn0Nm3QU3mbOHBshiOsjfgez3GrE0lHzH5ApJ+TIq54a76OeGrHTA1jbnZG0Cj6nmyUuYgVX/+86IwHGX6i3+RR+5qqKAFPMHmxrVI1RDgeUCITfZMyUlubGnHMiwbaf9NP6ehTIn2imN8q51PzRADXYnH71MWnROBdpqVtvNsNm6maOeqLlEJY2WI70z/X1cZA/Si7XL56dQ0EfJJ8/+lTDfq+T6sGW2iZ9alGWSnNj1kxJ0ICflzLxtyaSm3+HIa6v/9LFhA1vqCp8eHd+f39/93nwF5YS9XQAiVxg+1CsRvlUaEcKxSO2ZVU4BqrTmX5dvpQw+YyA+CeEy06bci5CLHFYjdaFBa2Zfmm4w47FT+OxNv+5SlYdWCyDEYigeTn9unypY0lW+08I4D/hPqTyi0uD4XFUbMbJvH79+pUn5Os4H+haKiFxYYjqyuAvt3Pllzho0uFsZOkz1jEsSL+NAPwNkwQM5Q2/ECDQodE2DROvXym5MVFexYDZQMOoCWOLkbgx3wf5JQ4suEiSGFkWjCdlg7LwsI36Pmxj6BgmzP5KptDEFDkSnS+vpoFsYUMmKmyooZTDK1PBqIrZSe1xWghR/CMA+G9M1YPFViq39hqNxl6LwpuALh7IclPiho0ALV2NhYG/IcDsp63EYW3Rp6hE0MFGN4ReT9LeCMiJH3mIJK444FBxBmLhNChTGUJaNV6OQgQ3Y0RS3hjqi6nIPUyza7W7CRBXKERgNoKl08T/YsI1J8kIC/8D+y0RBpEI3zSMNGxAI5JvqajFwkfIq97uOrggU0NEFu5S1P/pQqiQjfI6KT7EyP/IUt+kSeOExYg9uU2IicLa2ntM/LW0hkpZ+O+AECocYz3LOP4lKUY3bDiR2QYVP/2v7hYKH9ElCmndzdCjqOBI9bAK0+ID4ZvqhsALGyzEwqlbpSqcAsT0SXEfPej/1pHPhzhEFoCEB3AxljsSzdF49RLqbXBGqwBLEhO31LH/CntlkDSFHGmqFTgifFMdUg7HDxuq2/3XYMAHy/9iaiZe6qDNjOUr2RToCleNF3Qx1s55ELcor3EzR2TiqZVIWXi4IRsBUCGSWI08jC03bHAX4+YatMZpHQ7BjnmJ+FKmHJsd5IoAKMvXD9c5OTlG3oNtahM5HFEi9GwtSlRrYQ0kkxYqkWkmvRjDocj7jqHb1k0uMUSF97G0EFdzOIuRxAtwDqe0PAVfZkimLoCwMYWRMQ0quWaFzk1a14khclfjJVYTxBqnnPoGbJPuHEAzTR31X9BBA2YhR5joo+TRYCe5FrmWutrhpv4r3hAkHVo+Dc1DJpSyNuKMxwEqDx5A4nFv5cQIuWr0U/+oxYhDkLUzrOBsmkwvKLlAS1dq8AESFdo+QlFIg5CrxgbWS6Irxiq0nAcqVnCM0XWUUCDzFTw/ExHolVyHKdt0e8nNNMenOPzUfyX/qVqr4szS1trY+EcS2dPYidiom8s5DMJKCl+DwoFYv6IcbjxsqCufzz67X16FJz6TCPnlYDI9ksooX5ndvt0UroYKz1IP3YrxWNhQ897UCowviWlnGVtFJppGczX5JtizLX5NtQ7pk+Lcwp4+hcOtjM+0JpAh05TnsG2lFxz8UdlJq0IQjqW6FePqZ2539kN6hFg5tV39825e3u96AIexVajIERyPtxj7Gj9sZESIUzZupOEnTPKOJBmiaFduYrNvpXe7H7FkeYtx25yU+mdBCIFAcxnfhPuVe7fNzteb69jUW8nZUqUToXDeYtygHK4YyeHoSHYqhDDO5wWaiSkhsTk5yuy4P78PY0UPUb/AsdTyBA6XASHsOfTKwLHvPh5CYv5StFviWKrH4QbjixHnQFIhrGve0N0syhYjCHf4CKdyuLGwgeswFULYc+gNNM8UIEFY4SPkWiqPw6X3NEDHum7jLDWWSPeqPADCfW5s4YUNmvqHOVxqhDBWGMvP8OHlvoCMhwXlmiDUJ6RaXA5HU/9RDpcaIQ770tQ5lQqVXP4HKkfh5qLSA4Q3E/gBL2xc+u2bQI35lLy0bAv+wHYaFSrlHwL5MgpRyVWgETKRAcXncOqZkQphZj/zAyuh78mE6BmPkzkeL/Xvh1N/2OohXiUGiHPPdno/o3wZQRhSooxrfAqL5YYNM5T6D1JlwLCJwCvlp0kYQgi3Qgih8DA11eItxpbAjuCoK+4ugYQCbVYpg5/JvRpBWB79ZjyEfA5HU/8ijtipK0KaOk1Zz+ZniAK2GID5kBnIFbCQGLkWz1KZ9o26Ug3tPY4lbIktFcARO/0SjhboS6d4mskQLyQv9ael06RDpyVs6NAgmp7P5L6s/P333yvjIR8jvj0pHjI/y+NwftcftgpKSZv5MIrhmXaGwgRwtijeRpn3fswrT+ZwxQ93aar6zaCUP9usgop8Q+iEFJkfRgk39accDvaFaQkbiKtmUMqfSTc0jBCKB3b8yiM39Re8SmbS3tMhU2KbA0ClJyatPPLCBuVwyYk3U8qfiwohxZ+UWsSH6HK4pB1SOCTACzBzAJhTvorJGwBcDocuNWFRvxOU8ufhZ7AfJ35N6qO5CRV35o4rsPtsrn4GPLXGz/C5Em2psNUnYTiEUr5Gf2U+KqyAa0jTh4u0VDDTZMEC9umJBv16DipUFOBL6brFkRCxDZ8o/2VL+bOGl3N7VaKVsg0XweFw7iQJQPQzEg2gczBS+UHHJlXqNlwYYllImh3GLuWnEqWHZ7Nk6DOGLRWOO0w2iQElNulyTipUcjBGJgrp2v2ujEIER5NorK2uB6X8matQ6XVgcENKb6NUWEvFomcSzja/Uj5Zg9cOAKzESwwnSQCx5IgJOdssSvnRosj7Ng5yxsrtp4hvqXDaQ8QIOl/YUn6WtTImitx7rAgU4Cwu7HE44NCJDiKE+ow2ez+jyLlbAWenErTCpwi1VDxYJElyCJTRmKGfgclaRZZ7N5ZE93/czkSDKGiplpisol8XAp6e6dMpMIrmer9TcQ/vcK5nsAZ9IRBXuwkribBuXU+aXoUALde7fti/eWw6YsequBsWDTvFjO3kT6Ln6iTZbgk7utx1m/pelNzt41fH0iuSbRticIhOpRl/WCOuyDCVZiTZUBIcLuP5mcjRnokA5WbFEJkXJGGVQZceZ4+PMAgjaX6PSkfP9Mq93ethPxEDUYKRYYFuZBAMTfx625s9Pq/g00iKkO6Oober3FR0oxK7sAkfekOPZrHh1TIi0uz+zrUyD3zkwyA37CYqJGK+vOqrUB5CkE6Srcq3ePq+eDXcvtgolyBR0WcYIEYFE5WEueGetw7pFWAoRBATTBzK+/AbwcGVdSNuByaNyDBalbAng7700vMzCiTDhhBfhfIDBRg4NxjfTTPBH0sULGAkq3avuvEQjRQnJkSnlwCgiNsBmc8EUlXJlA1OANjTk1dK6247lV5hB4wgftWP5u9Cl01HLyaMd2UVXPOJN1qAVVmlVwHC+Len9OhhDCN9LngrxsTBmSwIraThHuQQawIUFBY2Y7cXlBzN30drs3VkxnNBiLeXfAYDXI3XnMV1OG3qxf88pQ8AtbBng2SlMhdXIz8aCVND/5kLtgtKaepizJKDS2W0MZqPLGkeC5G26MSkAOnUrBcAlV7Teoz3/OUb5AbjdcuWFrtjn0yon0lxKhb0OfyJM5Kax+Mj8i0AjDxICfpMUSPdGYXu1Yk6BWGalPDQNj+CxTMveQe5WuQ5Q3jCw+wjogyOPsUk1AvaeUpItAhAkdtpbswnIiLfSrfBuawlrdnKDzYu+miLAZo0e2qKlDnNQCLItpSsvSfvIECd9zyteVBTGRrlaV9nUnJ7J/HuSZH3scrEP5tqCN45SYoZ51Oxy5p6AzeeEKk/xqprKjla553wugYgEbMmbjJsCdTSHtvinmoixYiEivxAp8sm2Qv2lJ3ZFtB3cB4mNUD3VDq9OQUiiZY32IkQ9YlTZZDFpd5cGvnBSkYVvnCP/LCthwmLkeDbt21a550cd/eSzwdNFnkn0ypEqSNEUbvhlMgURe7dWrTmpLWnFNXhzQqzzC8UBXwh51yu2FKip4vp1u04byPwcg+P7hHrujD9k3CienZmiow0/dZmX4Zd9yiImwdoQHhCvu7tPFqSTUuh3TgvF7+YaRpMt+VmOSbCk0v6RlTRrgiP+w89uHjveuf20aq48ARDasYqA5W6wgyDPh5xkH7/PSv1tveKeFuqVGzLMioVyXtBBBTqY5/mD940/sDsZFFwXCVhhY0rrb4ZbLZnWxGiYYoJXjoJQ9VTNgHFBoiUe4ZvEVxt655NBujg3MlEywCIoCjMxEypm0m7+z767i7bjqHpukFE1zVNcPqNxEkLFLjSTCOOCdaOUlNuvpQ3GofDdnt42LhspTr9rTWrTB/ZzGzczIylk2IqOEKwnZaqdjF3uZBm4WuUa6xYNhaNJkpK2cfZ0I+CjabM7OctkAenHJv1hXZfsxzrNU9Zzc5rMKVYUhsFwfJ+lpqbO5q6jH6UChQzkm9BYAAqTTvrWwPmLBgw0pNTugjNhb67e4pAwEg//kwX4Xze+zgrwV2baZVIB+CXeBGiQH3fjrP5d1yUnsPvGyyRYB02jRIV5asdnhBYSgElxtrBHQYo44nG3/0lwcmlBHNu0YcnTRT5Rlt6L+NKI5U7pZNkmd7z9N0EpwSSEhuc7RKMmb9adj4CMTFhD0Pe6XL7y8souDMvSTmDNtBFa0kTinHZM2fbfl1CgRw2fv3bnRBIfyD5AqSFjj9mxYaS0SnNu6WTth47YtC6k5jtDV3fX/CU91h7SF2AmV6VtxBpSLH2ASt0kOzJaRAE3y87zdkouUd8mY/4BAG+aHWn5xhKr0nfWP6UvGgg4GwIs5kAUe7hblODM2e19IL0VOdXwBUa57O9/XexAueg83tRinKLMwJSP8OLcRctmNHq+5GDHkquj1MSZvr3jiyD4J6vqB2zivxg0UmXxqLvMZusYs9c+hqaSVLk3g2+DsTI9qr4ZZAW+hJDf3yAURa6/GQYQ6IvUe88USfKyqqBI2O27tzs9OCw7N7D/leRvg/viS9BT+pNOlUl2lqlItmVSkWngx9xxqyeiFxYYy9thR2n/ScbBcelvi1IBgtP1M3+E+FppVKpjlLmCX63VL9o6jjJIhqGLulOY5k9DGIit76aSMjPXzaG/Waz2d7eW9JyEyBLDGwUpCfu34huFw3KF4ItPbIpsBcPszQvcAzMRXrV+tzhuSAXpcn568+XBUH8fgBXF2Sp38tIiZkuBiBg/C6GurBl6IGcK8oliBcgpXnAhKC/aGAhSUXYIoAtF5+JlJLHTstxADMkvLTkwCZLaUwWfUfP8izP8izP8izP8izPsiTyf+qczmO5+Wa+AAAAAElFTkSuQmCC'
      })
    }
  }
}
</script>
```

Окружение разработки
--------------------

1. Установка зависимостей: 
```bash
   npm install
```
2. Запуск сборщика:
```bash
    npm start
```
3. Запуск финальной сборки
```bash
    npm run build
```