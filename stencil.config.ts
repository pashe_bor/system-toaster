import { reactOutputTarget } from '@stencil/react-output-target';
import { sass } from '@stencil/sass';

import { Config } from '@stencil/core';


export const config: Config = {
  namespace: 'system-toaster',
  outputTargets: [
    reactOutputTarget({
      componentCorePackage: '@karusel/system-toaster',
      proxiesFile: '../react/src/components.ts',
      includeDefineCustomElements: true,
    }),
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    }
  ],
  plugins: [sass()]
};
